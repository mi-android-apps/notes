# Notes

simple note taking app

### Objectiives
* create and editing simple text-based notes
* Saving data to a mobile device's persistent storage
* Easily discoverable patterns to accept user input

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen two")
![please find images under app-screenshots directory](app-screenshots/screen-3.png "screen three")
![please find images under app-screenshots directory](app-screenshots/screen-4.png "screen four")
![please find images under app-screenshots directory](app-screenshots/screen-5.png "screen five")
