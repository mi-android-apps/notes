package com.mndlovu.notes.data;

import android.net.Uri;
import android.provider.BaseColumns;

public final class NoteContract {

    public final static String CONTENT_AUTHORITY = "com.mndlovu.notes";
    public final static Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public final static String BASE_PATH = "notes";
    public static final String CONTENT_ITEM_TYPE = "Note";

    public NoteContract() {
    }

    /**
     * Inner class that defines constant values for the notes database table.
     * Each entry in the table represents a single pet.
     */
    public static final class NoteEntry implements BaseColumns {
        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, BASE_PATH);
        /** Name of database table for pets */
        public final static String TABLE_NAME = "notes";
        public final static String _ID = BaseColumns._ID;
        public static final String NOTE_TEXT = "noteText";
        public static final String NOTE_CREATED = "noteCreated";

        public static final String[] ALL_COLUMNS =
                {_ID, NOTE_TEXT, NOTE_CREATED};
    }
}
