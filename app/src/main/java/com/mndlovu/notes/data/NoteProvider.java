package com.mndlovu.notes.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mndlovu.notes.data.NoteContract.NoteEntry;

public class NoteProvider extends ContentProvider {

    private SQLiteDatabase database;
    // Constant to identify the requested operation
    private static final int NOTES = 1;
    private static final int NOTES_ID = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(NoteContract.CONTENT_AUTHORITY, NoteContract.BASE_PATH, NOTES);
        uriMatcher.addURI(NoteContract.CONTENT_AUTHORITY, NoteContract.BASE_PATH +  "/#", NOTES_ID);
    }


    @Override
    public boolean onCreate() {
        NoteDbHelper helper = new NoteDbHelper(getContext());
        database = helper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        if (uriMatcher.match(uri) == NOTES_ID) {
            selection = NoteEntry._ID + "=" + uri.getLastPathSegment();
        }

        return database.query(NoteEntry.TABLE_NAME, NoteEntry.ALL_COLUMNS,
                selection, null, null, null,
                NoteEntry.NOTE_CREATED + " DESC");
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long id = database.insert(NoteEntry.TABLE_NAME, null, values);
        return Uri.parse(NoteContract.BASE_PATH + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return database.delete(NoteEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return database.update(NoteEntry.TABLE_NAME, values, selection, selectionArgs);
    }
}
